package pages;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import utils.PageActionsUtils;

import java.time.Duration;
import java.util.List;

public class WishListPage extends PageObject {

    @Getter
    @FindAll({
            @FindBy(css = "sc-hOGjNT IyUir")
    })
    private List<WebElement> itemsPanels;

    @FindBy(id = "wishlist-dashboard-add-new-wish-button")
    private WebElement addNewWishButton;

    @FindBy(id = "wishlist-item-priority")
    private WebElement viewPriorityButton;

    @FindBy(id = "wishlist-item-title")
    private WebElement itemTitle;

    @FindBy(id = "wishlist-item-description")
    private WebElement itemDescription;

    @FindBy(id = "wishlist-item-price")
    private WebElement itemPrice;

    @FindBy(id = "wishlist-item-delete-button")
    private WebElement deleteButton;

    @FindBy(id="wishlist-item-edit-button")
    private WebElement editButton;

    public WishListPage(WebDriver driver) {
        super(driver);
    }

    public void waitItemTitleDisplayed(Duration duration) {
        PageActionsUtils.waitTillElementClickable(duration, itemTitle);
    }
}
