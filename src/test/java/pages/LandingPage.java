package pages;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LandingPage extends PageObject {

    @FindBy(id = "main-add-wishes-button")
    private WebElement addWishesNowButton;

    @FindBy(id = "main-wish-and-get-it-label")
    private WebElement generalLabel;

    @FindBy(id = "main-explicative-label")
    private WebElement explicativeLabel;

    public LandingPage(WebDriver driver) {
        super(driver);
    }
}