package pages;

import components.AddWishlistComponent;
import components.EditWishlistComponent;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import utils.PageActionsUtils;

import java.time.Duration;
import java.util.List;

@Getter
public class MainDashboardPage extends PageObject {

    private final AddWishlistComponent addWishlistComponent;
    private final EditWishlistComponent editWishlistComponent;

    @FindBy(id = "dashboard-add-wishlist-button")
    private WebElement addWishlistButton;

    @FindAll({
            @FindBy(css = "[class='sc-AjmZR jcpNfa']")
    })
    private List<WebElement> allWishlistsOnDashboard;

    public MainDashboardPage(WebDriver driver) {
        super(driver);
        this.addWishlistComponent = new AddWishlistComponent(driver);
        this.editWishlistComponent = new EditWishlistComponent(driver);
    }

    public void waitAddWishlistDisplayed(Duration duration) {
        PageActionsUtils.waitTillElementClickable(duration, addWishlistButton);
    }

    public void waitAllWishlistsOnDashboard(Duration duration) {
        PageActionsUtils.waitTillElementsDisplayed(duration, allWishlistsOnDashboard);
    }
}