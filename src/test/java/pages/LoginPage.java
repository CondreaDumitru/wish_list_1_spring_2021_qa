package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends PageObject {

    @FindBy(id = "login-title")
    private WebElement loginPageLabel;

    @FindBy(id = "login-email-form")
    private WebElement emailLoginField;

    @FindBy(id = "login-password-form")
    private WebElement passwordLoginField;

    @FindBy(id = "login-submit-button")
    private WebElement submit;

    @FindBy(linkText = "Register now")
    private WebElement registerNowButton;

    public LoginPage(WebDriver driver) {
        super(driver);
    }
}