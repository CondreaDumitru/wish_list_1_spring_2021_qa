package pages;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.PageActionsUtils;

import java.time.Duration;

public class RegisterPage extends PageObject {

    @FindBy(id = "fullname")
    private WebElement fullNameField;

    @FindBy(id = "email")
    private WebElement emailRegisterField;

    @FindBy(id = "password")
    private WebElement passwordRegisterField;

    @FindBy(id = "password2")
    private WebElement confirmPasswordField;

    @FindBy(id = "data-registration-form-create-account-button")
    private WebElement createAccount;

    @FindBy(linkText = "Log In")
    private WebElement loginLink;

    public RegisterPage(WebDriver driver) {
        super(driver);
    }

    public void waitTillFullNameClickable(Duration duration) {
        PageActionsUtils.waitTillElementClickable(duration, fullNameField);
    }

    public void waitTillCreateAccountClickable(Duration duration) {
        PageActionsUtils.waitTillElementClickable(duration, createAccount);
    }
}