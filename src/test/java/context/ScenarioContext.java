package context;

import java.util.HashMap;
import java.util.Map;


public class ScenarioContext {

    private static ScenarioContext scenarioContext;
    private final Map<Enum<?>, Object> context;

    private ScenarioContext() {
        this.context = new HashMap<>();
    }

    public static ScenarioContext getScenarioContext() {
        if (scenarioContext == null) {
            scenarioContext = new ScenarioContext();
        }
        return scenarioContext;
    }

    public void saveData(Enum<?> key, Object data) {
        getScenarioContext().context.put(key, data);
    }

    public Object getDataFrom(Fields key) {
        return getScenarioContext().context.get(key);
    }

    public String getDataAsString(Fields key) {
        return (String) getScenarioContext().context.get(key);
    }
}