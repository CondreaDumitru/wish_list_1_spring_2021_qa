package context;

public enum Fields {
    REGISTER_EMAIL,
    REGISTER_PASSWORD,
    REGISTER_FULL_NAME,
    TITLE,
    DESCRIPTION,
    RESPONSE_CODE
}
