package components;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.PageObject;
import utils.PageActionsUtils;

import java.time.Duration;

public class AddItemComponent extends PageObject {

    @FindBy(css = "#root > div:nth-child(2) > div > div")
    private WebElement addNewWishForm;

    @FindBy(id = "wishlist-dashboard-modal-label-wish-name")
    private WebElement addNewWishLabel;

    @FindBy(id = "wishlist-dashboard-modal-label-link")
    private WebElement wishNameLabel;

    @FindBy(id = "wishlist-dashboard-modal-label-link")
    private WebElement linkLabel;

    @FindBy(id = "wishlist-dashboard-modal-label-price")
    private WebElement priceLabel;

    @FindBy(id = "wishlist-dashboard-modal-label-currency")
    private WebElement currencyLabel;

    @FindBy(id = "wishlist-dashboard-modal-label-description")
    private WebElement descriptionLabel;

    @FindBy(id = "wishlist-dashboard-modal-label-priority")
    private WebElement priorityLabel;

    @FindBy(id = "wishlist-dashboard-modal-input-wish-name")
    private WebElement wishNameField;

    @FindBy(id = "wishlist-dashboard-modal-input-link")
    private WebElement linkField;

    @FindBy(id = "wishlist-dashboard-modal-input-price")
    private WebElement priceField;

    @FindBy(id = "wishlist-dashboard-modal-input-currency")
    private WebElement currencyDropdown;

    @FindBy(id = "wishlist-dashboard-modal-input-description")
    private WebElement descriptionField;

    @FindBy(id = "wishlist-dashboard-modal-input-priority")
    private WebElement priorityDropdown;

    @FindBy(css = "#root > div:nth-child(2) > div > div > button")
    private WebElement addWishButton;

    @FindBy(id = "wishlist-dashboard-modal-exit")
    private WebElement closeWishFormButton;

    public AddItemComponent(WebDriver driver) {
        super(driver);
    }

    public void waitAddWishDisplayed(Duration duration) {
        PageActionsUtils.waitTillElementClickable(duration, addWishButton);
    }
}