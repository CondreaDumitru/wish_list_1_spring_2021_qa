package components;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.PageObject;

public class AddWishlistComponent extends PageObject {

    @FindBy(id = "data-wishlist-form")
    private WebElement wishlistForm;

    @FindBy(id = "close-wishlist-form")
    private WebElement closeFormXButton;

    @FindBy(id = "wishlist-form-label")
    private WebElement formLabel;

    @FindBy(id = "data-wishlist-form-title")
    private WebElement wishlistTitle;

    @FindBy(id = "data-wishlist-form-type")
    private WebElement wishlistType;

    @FindBy(id = "data-wishlist-form-date")
    private WebElement wishlistDate;

    @FindBy(id = "data-wishlist-form-description")
    private WebElement wishlistDescription;

    @FindBy(id = "data-wishlist-form-privacy")
    private WebElement wishlistPrivacy;

    @FindBy(id = "data-wishlist-form-save-button")
    private WebElement saveWishlistButton;

    public AddWishlistComponent(WebDriver driver) {
        super(driver);
    }
}