package components;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.PageObject;
import utils.PageActionsUtils;

import java.time.Duration;

public class EditWishlistComponent extends PageObject {

    @FindBy(css = "[class='sc-fKVsgm bTesjL']")
    private WebElement editWishlistForm;

    @FindBy(id = "close-wishlist-form")
    private WebElement closeEditFormXButton;

    @FindBy(id = "data-wishlist-form-title")
    private WebElement editWishlistTitle;

    @FindBy(id = "data-wishlist-form-type")
    private WebElement editWishlistType;

    @FindBy(id = "data-wishlist-form-date")
    private WebElement editWishlistDate;

    @FindBy(id = "data-wishlist-form-description")
    private WebElement editWishlistDescription;

    @FindBy(id = "data-wishlist-form-privacy")
    private WebElement editWishlistPrivacy;

    @FindBy(id = "data-wishlist-form-save-button")
    private WebElement saveChangesButton;

    @FindBy(css = "[class='sc-llYToB dEgymt']")
    private WebElement deleteWishlistButton;

    @FindBy(css = "[class='Modal__wrap-jkYgUh sumng]")
    private WebElement deleteWishlistForm;

    @FindBy(css = "[class='sc-cTApHj eodRAs']")
    private WebElement deleteWishlistMessage;

    @FindBy(css = "[class='sc-dPiKHq cdBnjv']")
    private WebElement continueDeleteButton;

    @FindBy(css = "[class='sc-dPiKHq chxoOn']")
    private WebElement cancelDeleteButton;

    public EditWishlistComponent(WebDriver driver) {
        super(driver);
    }

    public void waitDeleteButtonDisplayed(Duration duration) {
        PageActionsUtils.waitTillElementDisplayed(duration, deleteWishlistButton);
    }
}
