package components;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.PageObject;

public class FooterComponent extends PageObject {

    @FindBy(id = "footer")
    private WebElement footer;

    @FindBy(css = "#footer > a:nth-child(2)")
    private WebElement facebookButton;

    @FindBy(css = "#footer > a:nth-child(3)")
    private WebElement instagramButton;

    @FindBy(id = "footer-copyright")
    private WebElement copyright;

    @FindBy(id = "footer-contact-email")
    private WebElement emailContact;

    public FooterComponent(WebDriver driver) {
        super(driver);
    }
}