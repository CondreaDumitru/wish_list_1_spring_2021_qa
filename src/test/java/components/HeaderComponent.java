package components;

import lombok.Getter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.PageObject;
import utils.PageActionsUtils;

import java.time.Duration;

public class HeaderComponent extends PageObject {

    @FindBy(id = "header")
    private WebElement header;

    @FindBy(id = "header-logo")
    private WebElement logoOnHeader;

    @FindBy(id = "header-site-name")
    private WebElement siteNameLabel;

    @FindBy(id = "header-main-page-link")
    private WebElement mainPageButton;

    @FindBy(id = "header-about-us-link")
    private WebElement aboutUsButton;

    @FindBy(id = "header-register-button")
    private WebElement registerButton;

    @FindBy(id = "header-login-button")
    private WebElement loginButton;

    @FindBy(css = "button[class='sc-iCfLBT sc-hBURRC cvReAN hgarYh']")
    private WebElement logOutButton;

    @FindBy(id = "header-dashboard-link")
    private WebElement dashboardButton;

    public HeaderComponent(WebDriver driver) {
        super(driver);
    }

    public void waitMainPageClickable(Duration duration) {
        PageActionsUtils.waitTillElementClickable(duration, mainPageButton);
    }

    public void waitRegisterButtonClickable(Duration duration) {
        PageActionsUtils.waitTillElementClickable(duration, registerButton);
    }

    public void waitLoginButtonClickable(Duration duration) {
        PageActionsUtils.waitTillElementClickable(duration, loginButton);
    }

    public void waitDashboardButtonDisplayed(Duration duration) {
        PageActionsUtils.waitTillElementDisplayed(duration, dashboardButton);
    }
}