package hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.assertj.core.api.SoftAssertions;
import stepdefs.frontEnd.general.BaseSteps;

import static utils.WebDriverInitializer.getWebInitInstance;
import static utils.WebDriverInitializer.setupWebDriver;

public class Hooks extends BaseSteps {

    @Before("@FE")
    public void setup() {
        setupWebDriver();
        setDriver();
    }

    @After("@FE")
    public void teardown() {
        getWebInitInstance().quit();
    }

    @After
    public void assertAfterAllTests() {
        softly.assertAll();
    }
}