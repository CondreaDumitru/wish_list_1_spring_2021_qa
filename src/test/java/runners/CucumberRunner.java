package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import static configuration.RestConfig.setUpBaseTestConfig;
import static utils.WebAwakener.awaken;
import static utils.WebDriverInitializer.setupWebDriver;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = ("com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"),
        stepNotifications = true,
        features = "src/test/resources/features",
        glue = {("stepdefs"), ("hooks")},
        tags = ("@Run")
)

public class CucumberRunner {

    @BeforeClass
    public static void setup() {
        setUpBaseTestConfig();
        setupWebDriver();
        //awaken();
    }
}
