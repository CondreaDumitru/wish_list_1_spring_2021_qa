package utils;

import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static stepdefs.frontEnd.general.BaseSteps.isPresent;
import static utils.WebDriverInitializer.getWebInitInstance;
import static utils.WishlistConstants.BASE_URL;

@Log4j2
public class WebAwakener {

    public static void awaken() {
        for (int x = 0; x < 3; x++) {
            WebDriver driver = getWebInitInstance().getDriver();
            driver.navigate().to(BASE_URL);
            driver.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
            if (isPresent("login button")) {
                getWebInitInstance().quit();
                log.info("Website awakened");
                return;
            }
        }
        log.error("Website is not responding after 3 attempts");
        System.exit(1);
    }
}
