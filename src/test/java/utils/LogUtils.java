package utils;

import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;

public class LogUtils {
    public static String getWebElementInfo(WebElement element) {
        String info = "";
        try {
            if (element != null) {
                if (element.getAttribute("id") != null) {
                    info = info + element.getAttribute("id");
                }
                info = info + " - " + element.getText() + " " + element.getTagName();
            }
        } catch (StaleElementReferenceException e) {
        }
        return info;
    }
}