package utils;

public class StringProcessing {
    public static String snakeToCamel(String snake){
        return snake.replaceFirst("_[a-z]", String.valueOf(Character.toUpperCase(snake.charAt(snake.indexOf("_") + 1))));
    }
}
