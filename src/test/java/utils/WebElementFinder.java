package utils;

import lombok.SneakyThrows;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.lang.reflect.Field;

import static utils.PackageReader.getListOfClassesFrom;

public class WebElementFinder {

    @SneakyThrows
    public static WebElement getWebElementFieldOfName(String name, WebDriver driver) {
        for (Class<?> aClass : getListOfClassesFrom("pages", "components")) {
            for (Field field : aClass.getDeclaredFields()) {
                if (name.replaceAll("\\s", "").equalsIgnoreCase(field.getName())) {
                    field.setAccessible(true);
                    return (WebElement) field.get(aClass.getDeclaredConstructor(WebDriver.class)
                            .newInstance(driver));
                }
            }
        }
        throw new NoSuchFieldException("Can't find [" + name + "] WebElement in <pages> and <components> packages!");
    }
}