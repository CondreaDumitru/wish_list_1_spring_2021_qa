package utils;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

import static utils.WebDriverInitializer.getWebInitInstance;

public class PageActionsUtils {
    public static void waitTillElementClickable(Duration duration, WebElement webElement) {
        new WebDriverWait(getWebInitInstance().getDriver(), duration.toSeconds())
                .until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public static void waitTillElementDisplayed(Duration duration, WebElement webElement) {
        new WebDriverWait(getWebInitInstance().getDriver(), duration.toSeconds())
                .until(ExpectedConditions.visibilityOf(webElement));
    }

    public static void waitTillElementsDisplayed(Duration duration, List<WebElement> webElements) {
        new WebDriverWait(getWebInitInstance().getDriver(), duration.toSeconds())
                .until(ExpectedConditions.visibilityOfAllElements(webElements));
    }
}