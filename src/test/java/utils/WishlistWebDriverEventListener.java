package utils;

import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

@Log4j2
public class WishlistWebDriverEventListener extends AbstractWebDriverEventListener {

    @Override
    public void afterNavigateTo(String url, WebDriver driver) {
        TakeScreenshotUtils.takeScreenshot(driver);
        log.info("[Navigated] to URL: " + url);
    }

    @Override
    public void afterClickOn(WebElement webElement, WebDriver driver) {
        TakeScreenshotUtils.takeScreenshot(driver);
        log.info("[Clicked] on element: " + LogUtils.getWebElementInfo(webElement));
    }

    @Override
    public void afterChangeValueOf(WebElement webElement, WebDriver driver, CharSequence[] keysToSend) {
        TakeScreenshotUtils.takeScreenshot(driver);
        log.info("[Changed] value of element: " + LogUtils.getWebElementInfo(webElement));
    }
}