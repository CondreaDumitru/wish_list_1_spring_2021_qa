package utils;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

public class TakeScreenshotUtils {
    public static void takeScreenshot(WebDriver driver) {
        String screenshotDateTime = LocalDateTime.now().toString().replaceAll(":","-")
                .replaceAll("\\.","_");
        File screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            String fileSeparator = java.nio.file.FileSystems.getDefault().getSeparator();
            FileUtils.copyFile(screenShot, new File(System.getProperty("user.dir")
                    + fileSeparator + "target" + fileSeparator + "screenshots" + fileSeparator + screenshotDateTime + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}