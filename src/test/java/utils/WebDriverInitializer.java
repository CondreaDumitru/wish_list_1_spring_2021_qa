package utils;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.Getter;
import lombok.SneakyThrows;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import static utils.PropertyReader.readProperty;

public class WebDriverInitializer {

    private static WebDriverInitializer instance;
    private WebDriver driver;
    @Getter
    private SoftAssertions softly;

    /**
     * setup browser in dependence of property from test_configuration.properties
     */
    public static void setupWebDriver() {
        if ("firefox".equals(readProperty("browser"))) {
            WebDriverManager.firefoxdriver().setup();
        } else {
            WebDriverManager.chromedriver().setup();
        }
    }

    /**
     * WebDriverInitializer constructor
     */
    private WebDriverInitializer() {
        this.driver = getDriver();
    }

    /**
     * create instance of WebDriverInitializer class with singleton pattern
     *
     * @return WebDriverInitializer instance
     */
    public static WebDriverInitializer getWebInitInstance() {
        if (instance == null) {
            instance = new WebDriverInitializer();
        }
        return instance;
    }

    /**
     * singleton to get WebDriver
     *
     * @return WebDriver field of WebDriverInitializer instance
     */
    public WebDriver getDriver() {
        if (driver == null) {
            driver = initializeWebDriver();
            EventFiringWebDriver eventFiringDriver = new EventFiringWebDriver(driver);
            WishlistWebDriverEventListener eventListener = new WishlistWebDriverEventListener();
            eventFiringDriver.register(eventListener);
            driver = eventFiringDriver;
        }
        return driver;
    }

    public SoftAssertions getAssert(){
        return this.softly = new SoftAssertions();
    }

    /**
     * initialize WebDriver
     *
     * @return WebDriver that will be used
     */
    public static WebDriver initializeWebDriver() {
        if ("firefox".equals(readProperty("browser"))) {
            FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.setHeadless(Boolean.parseBoolean(readProperty("headless")));
            firefoxOptions.setAcceptInsecureCerts(true);
            firefoxOptions.addArguments("--disable-gpu", "--disable-extensions", "--window-size=1920,1080");
            return new FirefoxDriver(firefoxOptions);
        }
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setHeadless(Boolean.parseBoolean(readProperty("headless")));
        chromeOptions.addArguments("--disable-gpu",
                "--ignore-certificate-errors",
                "--disable-extensions",
                "--window-size=1920,1080");
        return new ChromeDriver(chromeOptions);
    }

    @SneakyThrows
    public void quit() {
        if (driver instanceof ChromeDriver) {
            driver.close();
            driver.quit();
        } else {
            driver.close();
        }
        driver = null;
    }
}