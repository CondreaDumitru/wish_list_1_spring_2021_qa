package utils;

import lombok.extern.log4j.Log4j2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

@Log4j2
public class PropertyReader {
    public static String readProperty(String property) {
        String propertyValue = "Property not found";
        try {
            Properties properties = new Properties();
            Path pathToProperties = Paths.get("src", "test", "resources", "test_configuration.properties");

            InputStream fileInputStream = new FileInputStream(pathToProperties.toString());
            properties.load(fileInputStream);
            propertyValue = properties.getProperty(property);
        } catch (FileNotFoundException e) {
            log.warn("Properties file is missing");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return propertyValue;
    }
}