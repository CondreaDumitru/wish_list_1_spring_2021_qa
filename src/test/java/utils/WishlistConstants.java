package utils;

import dbservice.DBService;

import java.time.Duration;

import static dbservice.DBService.*;
import static utils.PropertyReader.readProperty;

public class WishlistConstants {

    public static final String BASE_URL = readProperty("baseURL");
    public static final String BACKEND_URL = readProperty("BE_baseURL");
    public static final String MAIN_DASHBOARD_END_POINT = "/main-dashboard";
    public static final String WISHLIST_MAIN_DASHBOARD_END_POINT = "/wishlist-dashboard";
    public static final String LOGIN_END_POINT = "/login";
    public static final String REGISTER_END_POINT = "/register";
    public static final String REGISTER_SUCCESS_END_POINT = "/register/success";
    public static final String WISHLISTS_END_POINT = "/wishlists";
    public static final String WISHLIST_END_POINT = "/wishlist";
    public static final String WISHES_END_POINT = "/wishes";

    public static final String WEBSITE_TITLE = "GoldWish";

    public static final String LOGIN_FORM_LABEL = "Log In to Your GoldWish Account";
    public static final String LOGIN_SUBMIT_BUTTON_TEXT = "Log In";
    public static final String REGISTER_NOW_BUTTON_TEXT_AT_LOGIN = "Register now";
    public static final String DELETE_POPUP_MESSAGE = "Are you sure you want to delete this wishlist?";

    public static final String REGISTRATION_SUCCESS_MESSAGE = "Congratulations,\nYour registration was successful!";
    public static final String DELETE_WISHLIST_MESSAGE = "Are you sure you want to delete this wishlist?";

    public static final Duration DURATION_10_SECONDS = Duration.ofSeconds(10L);
    public static final Duration DURATION_20_SECONDS = Duration.ofSeconds(20L);

    public static final Long LONG_WAIT_10000 = 10000L;

    public static final String REGISTER_BUTTON_NAME = "Register";
    public static final String LOGIN_BUTTON_NAME = "Login Button";
    public static final String SUBMIT_BUTTON_NAME = "submit";

    //Credentials for test existing account
    public static final int TEST_USER_ID = getUserIdByEmail(readProperty("email"));
    public static final String TEST_USER_FULL_NAME = readProperty("fullName");
    public static final String TEST_USER_EMAIL = readProperty("email");
    public static final String TEST_USER_PASSWORD = readProperty("password");

    //Credentials for register tests (only email, because only by email users is unique)
    public static final String REGISTER_TEST_USER_EMAIL = readProperty("register_email");

    //Template test data to create wishlists
    public static final String WISHLIST_TITLE = "TestTitle";
    public static final String WISHLIST_DATE = "2021-05-01";
    public static final String WISHLIST_DESCRIPTION = "TestDesc";
    public static final String WISHLIST_PRIVACY = "PUBLIC";
    public static final String WISHLIST_TYPE = "christmas";

    //Template test data to create wishes
    public static final String DESCRIPTION = "Test Description";
    public static final String LINK = "https://www.endava.com/";
    public static final String PRICE = "1200";
    public static final String TITLE = "test title";
}