package configuration;

import configuration.dto.UserDTO;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import lombok.Getter;
import org.hamcrest.Matchers;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static utils.WishlistConstants.*;

@Getter
public abstract class RestConfig {

    public static void setUpBaseTestConfig() {
        RequestSpecification request = new RequestSpecBuilder()
                .setBaseUri(BACKEND_URL)
                .setContentType(JSON)
                .setAccept(JSON)
                .build()
                .log().all();
        RestAssured.requestSpecification = request;

        String token = given()
                .body(UserDTO.builder()
                        .email(TEST_USER_EMAIL)
                        .password(TEST_USER_PASSWORD)
                        .build())
                .post(LOGIN_END_POINT)
                .path("token");
        RestAssured.requestSpecification = request.auth().oauth2(token);

        ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder();
        responseSpecBuilder.expectResponseTime(Matchers.lessThan(LONG_WAIT_10000));
        RestAssured.responseSpecification = responseSpecBuilder.build();
    }
}