package configuration.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WishlistItemsDTO {
    private String currency;
    private String description;
    private String image;
    private String link;
    private Long price;
    private String priority;
    private String title;
}
