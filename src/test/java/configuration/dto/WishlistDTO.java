package configuration.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WishlistDTO {
    private String title;
    private String type;
    private String description;
    private String date;
    private String privacy;
}
