package configuration.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserDTO {
    public String fullName;
    public String email;
    public String password;
}
