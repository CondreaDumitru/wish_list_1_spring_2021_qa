package dbservice;

import lombok.SneakyThrows;
import utils.StringProcessing;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;

public class DbUtils {

    @SneakyThrows
    public static <T> ArrayList<T> resultToEntity(ResultSet resultSet, Class<T> entityClass) {
        // Create a List of type T. It will contain objects of Entity classes
        ArrayList<T> list = new ArrayList<>();
        // Get the information about the columns of resultSet
        ResultSetMetaData metaData = resultSet.getMetaData();
        // Traverse the resultSet
        while (resultSet.next()) {
            // Get an instance of the Class with reflection
            T object = entityClass.getConstructor().newInstance();
            // traverse each column
            for (int i = 0; i < metaData.getColumnCount(); i++) {
                // Get the name of the column in db
                String columnName = metaData.getColumnLabel(i + 1);
                //Get the name of the column in Entity Classes
                String entityColumnName = StringProcessing.snakeToCamel(metaData.getColumnLabel(i + 1));

                //Receiving the field of column in Entity Class
                Field field = entityClass.getDeclaredField(entityColumnName);

                //Building the name of the setter for the current field in Entity Class
                String setName = "set" + entityColumnName.toUpperCase().charAt(0) + entityColumnName.substring(1);

                // Setting up the setter method
                Method setMethod = entityClass.getMethod(setName, field.getType());

                /*
                  If the column contains not null, value of column is assigned to specific field in entity Class
                  If the column contains null, it is assigned "null" string to specific field in Entity Class
                  This is done to avoid setting null value into the field
                */
                if (resultSet.getObject(columnName) != null) {
                    setMethod.invoke(object, resultSet.getObject(columnName).toString());
                } else {
                    setMethod.invoke(object, "null");
                }
            }
            // Add the assigned object to the list collection
            list.add(object);
        }
        // return list
        return list;
    }
}
