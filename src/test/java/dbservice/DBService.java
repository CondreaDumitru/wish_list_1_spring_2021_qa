package dbservice;

import lombok.SneakyThrows;
import utils.PropertyReader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Locale;

import static dbservice.DbUtils.resultToEntity;
import static dbservice.Queries.*;

public class DBService {
    static final String DB_URL = PropertyReader.readProperty("db_url");
    static final String USER = PropertyReader.readProperty("user");
    static final String PASS = PropertyReader.readProperty("pass");

    @SneakyThrows
    public static ArrayList<?> runQuery(String query, Class<?> classVariable) {
        Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
        Statement stmt = conn.createStatement();
        String lowerQuery = query.toLowerCase(Locale.ROOT);

        if (lowerQuery.contains("insert") || lowerQuery.contains("update") || lowerQuery.contains("delete")) {
            stmt.executeUpdate(query);
            return null;
        } else {
            ResultSet resultSet = stmt.executeQuery(query);
            return resultToEntity(resultSet, classVariable);
        }
    }

    @SneakyThrows
    public static ResultSet runQueryWithResultSet(String query) {
        Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
        Statement stmt = conn.createStatement();
        String lowerQuery = query.toLowerCase(Locale.ROOT);

        if (lowerQuery.contains("insert") || lowerQuery.contains("update") || lowerQuery.contains("delete")) {
            stmt.executeUpdate(query);
            return null;
        } else {
            return stmt.executeQuery(query);
        }
    }

    public static void deleteUserByID(String id) {
        String query = String.format(DELETE_USER_BY_ID, id);
        runQuery(query, null);
    }

    public static void deleteUserByEmail(String email) {
        String query = String.format(DELETE_USER_BY_EMAIL, email);
        runQuery(query, null);
    }

    public static void deleteUsersItems(int id) {
        String query = String.format(DELETE_USER_ITEMS, id);
    }

    @SneakyThrows
    public static int getUserIdByEmail(String email) {
        String query = String.format(GET_USER_ID_BY_EMAIL, email);
        ResultSet rs = runQueryWithResultSet(query);
        if (rs != null && rs.next()) {
            return rs.getInt("id");
        }
        return 0;
    }
}
