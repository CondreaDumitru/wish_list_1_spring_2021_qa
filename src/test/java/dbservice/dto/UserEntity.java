package dbservice.dto;

import lombok.Data;

@Data
public class UserEntity {
    private String id;
    private String fullName;
    private String email;
    private String password;
    private String roleId;
}
