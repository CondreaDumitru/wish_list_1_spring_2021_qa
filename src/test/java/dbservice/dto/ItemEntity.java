package dbservice.dto;

import lombok.Data;

@Data
public class ItemEntity {
    public String id;
    public String eventId;
    public String title;
    public String description;
    public String image;
    public String deleted;
    public String link;
    public String price;
    public String currency;
    public String priority;
}
