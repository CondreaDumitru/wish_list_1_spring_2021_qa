package dbservice.dto;

import lombok.Data;

@Data
public class WishlistEntity {
    private String id;
    private String userId;
    private String type;
    private String description;
    private String date;
    private String privacy;
    private String title;
    private String deleted;
}
