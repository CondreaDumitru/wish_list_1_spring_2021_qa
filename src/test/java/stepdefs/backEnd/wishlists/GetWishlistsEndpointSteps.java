package stepdefs.backEnd.wishlists;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import stepdefs.frontEnd.general.BaseSteps;

import java.util.ArrayList;
import java.util.Map;

import static context.Fields.DESCRIPTION;
import static context.Fields.TITLE;
import static context.ScenarioContext.getScenarioContext;
import static stepdefs.backEnd.general.Requests.*;
import static utils.WishlistConstants.*;

public class GetWishlistsEndpointSteps extends BaseSteps {

    @Given("user send post request to save wishlist")
    public void userSendPostRequestToSaveWishlist() {
        String title = "Test111T1tle";
        String description = "Desc00123";
        addWishlistForUser(TEST_USER_EMAIL, title, description);
        getScenarioContext().saveData(TITLE, title);
        getScenarioContext().saveData(DESCRIPTION, description);
    }

    @Then("at get request wishlist with saved data is returned")
    public void atGetRequestWishlistWithSavedDataIsReturned() {
        ArrayList<Map<String, ?>> wishlists = getAllWishlists().jsonPath().get();
        for (Map<String, ?> wishlist : wishlists) {
            if (getScenarioContext().getDataFrom(TITLE).toString().equals(wishlist.get("title"))) {
                softly.assertThat(wishlist.get("date"))
                        .describedAs("Date don't match").isEqualTo(WISHLIST_DATE);
                softly.assertThat(wishlist.get("description"))
                        .describedAs("Description don't match").isEqualTo(getScenarioContext().getDataFrom(DESCRIPTION).toString());
                softly.assertThat(wishlist.get("privacy"))
                        .describedAs("Privacy don't match").isEqualTo(WISHLIST_PRIVACY);
                softly.assertThat(wishlist.get("type"))
                        .describedAs("Privacy don't match").isEqualTo(WISHLIST_TYPE);
            }
        }
        deleteAllCreatedWishlists();
    }
}