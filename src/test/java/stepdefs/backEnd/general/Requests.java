package stepdefs.backEnd.general;

import com.google.gson.JsonObject;
import configuration.dto.UserDTO;
import configuration.dto.WishlistDTO;
import configuration.dto.WishlistItemsDTO;
import context.Fields;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.apache.http.HttpStatus;
import stepdefs.frontEnd.general.BaseSteps;

import java.util.ArrayList;
import java.util.Map;

import static context.Fields.*;
import static io.restassured.RestAssured.given;
import static utils.WishlistConstants.*;

@Log4j2
public class Requests extends BaseSteps {
    public static JsonObject requestParams = new JsonObject();

    @SneakyThrows
    public static void registerRequest(Map<Enum<Fields>, String> credentials) {
        Response response = given()
                .body(UserDTO.builder()
                        .email(credentials.get(REGISTER_EMAIL))
                        .fullName(credentials.get(REGISTER_FULL_NAME))
                        .password(credentials.get(REGISTER_PASSWORD))
                        .build())
                .post(BACKEND_URL + REGISTER_END_POINT);
    }

    public static void addWishlistForUser(String email, String title, String description) {
        Response response = given()
                .body(WishlistDTO.builder()
                        .title(title)
                        .date(WISHLIST_DATE)
                        .description(description)
                        .privacy(WISHLIST_PRIVACY)
                        .type(WISHLIST_TYPE)
                        .build())
                .post(WISHLISTS_END_POINT + "/" + email + WISHLIST_END_POINT);
        softly.assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_OK);
        log.info("Created wishlist for test user");
    }

    public static Response getAllWishlists() {
        log.info("User wishlist requested");
        Response response = given().get(WISHLISTS_END_POINT);
        softly.assertThat(response.statusCode()).isEqualTo(HttpStatus.SC_OK);
        return response;
    }

    public static void deleteAllCreatedWishlists() {
        log.info("Created during test wishlists are deleting");
        Response response = getAllWishlists();
        ArrayList<Map<String, ?>> wishlists = response.jsonPath().get();
        wishlists.forEach(wishlist -> deleteWishlistById((Integer) wishlist.get("id")));
    }

    public static int getWishlistIdByTitle(String title) {
        Response response = getAllWishlists();
        ArrayList<Map<String, ?>> wishlists = response.jsonPath().get();

        for (Map<String, ?> wishlist : wishlists)
            if (wishlist.get("title").equals(title)) {
                return (int) wishlist.get("id");
            }
        return -1;
    }

    public static void deleteWishlistById(Integer id) {
        given().delete(WISHLISTS_END_POINT + "/" + id);
    }

    public static void deleteWishById(Integer id) {
        given().delete(WISHES_END_POINT + "/" + id);
    }

    public static Response getAllWishes() {
        return given().get(WISHES_END_POINT);
    }

    public static Response addItem(WishlistItemsDTO itemsDTO, int id) {
        return given()
                .body(itemsDTO)
                .post(WISHES_END_POINT + "/" + id);
    }
}
