package stepdefs.backEnd.wishlistItems;

import configuration.dto.WishlistItemsDTO;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import stepdefs.frontEnd.general.BaseSteps;

import static context.Fields.RESPONSE_CODE;
import static context.ScenarioContext.getScenarioContext;
import static stepdefs.backEnd.general.Requests.*;
import static utils.WishlistConstants.WISHLIST_TITLE;

public class AbilityToSaveItemSteps extends BaseSteps {

    @When("user send post request to add a wish by filling \"Title\" field")
    public void userSendPostRequestToAddAWishByFillingField() {
        WishlistItemsDTO itemsDTO = WishlistItemsDTO
                .builder()
                .title("Title")
                .build();

        getScenarioContext().saveData(RESPONSE_CODE, addItem(itemsDTO, getWishlistIdByTitle(WISHLIST_TITLE)).getStatusCode());
        deleteWishlistById(getWishlistIdByTitle(WISHLIST_TITLE));
    }

    @Given("user send post request to add a wish without filling \"Title\" field")
    public void userSendPostRequestToAddAWishWithoutFillingField() {
        WishlistItemsDTO itemsDTO = WishlistItemsDTO
                .builder()
                .build();

        getScenarioContext().saveData(RESPONSE_CODE,
                addItem(itemsDTO, getWishlistIdByTitle(WISHLIST_TITLE)).getStatusCode());
    }

    @Then("user get response with code {}")
    public void userGetResponseWithCode(int code) {
        softly.assertThat(getScenarioContext().getDataFrom(RESPONSE_CODE))
                .describedAs("Response code does not match expected one").isEqualTo(code);
    }
}
