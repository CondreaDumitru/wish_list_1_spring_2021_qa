package stepdefs.frontEnd.general;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.PageActionsUtils;
import utils.WishlistConstants;

import java.time.LocalDate;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static context.Fields.DESCRIPTION;
import static context.Fields.TITLE;
import static dbservice.DBService.*;
import static context.ScenarioContext.getScenarioContext;
import static stepdefs.backEnd.general.Requests.deleteAllCreatedWishlists;
import static utils.WebElementFinder.*;
import static utils.WishlistConstants.*;

public class CommonSteps extends BaseSteps {
    @When("user clicks on {}")
    public void userClickOnButton(String buttonName) {
        WebElement webElement = getWebElementFieldOfName(buttonName, driver);
        PageActionsUtils.waitTillElementClickable(DURATION_10_SECONDS, webElement);
        click(buttonName);
    }

    @When("user populates {} with {}")
    public void userPopulates(String fieldName, String keys) {
        populate(fieldName, keys);
    }

    @Then("{} is displayed")
    public void IsDisplayed(String name) {
        softly.assertThat(isPresent(name)).describedAs(name + "is not displayed").isTrue();
    }

    @Given("user is on Main Page")
    public void userIsOnMainPage() {
        navigateToMainPage();
        driver.manage().timeouts().implicitlyWait(DURATION_10_SECONDS.toSeconds(), TimeUnit.SECONDS);
    }

    @Given("user is on log in page")
    public void userIsOnLogInPage() {
        navigateToMainPage();
        getHeaderComponent().waitLoginButtonClickable(DURATION_20_SECONDS);
        click("Login button");
    }

    @Given("user is on register page")
    public void userIsOnRegisterPage() {
        navigateToMainPage();
        getHeaderComponent().waitRegisterButtonClickable(DURATION_20_SECONDS);
        click("Register Button");
    }

    @Given("user is logged in with test account")
    public void userIsLoggedInWithCredentials() {
        navigateToMainPage();
        click("Login button");
        populate("emailLoginField", TEST_USER_EMAIL);
        populate("passwordLoginField", TEST_USER_PASSWORD);
        click("Submit");
        getMainDashboardPage().waitAddWishlistDisplayed(DURATION_10_SECONDS);
    }

    @When("user log in with credentials")
    public void userLogInWithCredentials(DataTable dataTable) {
        List<String> dataList = dataTable.asList();
        populate("emailLoginField", dataList.get(0));
        populate("passwordLoginField", dataList.get(1));
        click("submit");
    }

    @Then("user is redirected to main dashboard")
    public void userIsRedirectedToMainDashboard() {
        getMainDashboardPage().waitAddWishlistDisplayed(DURATION_10_SECONDS);
        softly.assertThat(driver.getCurrentUrl())
                .describedAs("User is not redirected to main dashboard.")
                .isEqualTo(BASE_URL + MAIN_DASHBOARD_END_POINT);
    }

    @When("new wishlist is created with title {} and description {}")
    @SneakyThrows
    public void newWishlistIsCreated(String title, String description) {
        Thread.sleep(3000);
        deleteAllCreatedWishlists();
        driver.navigate().refresh();
        getScenarioContext().saveData(TITLE, title);
        getScenarioContext().saveData(DESCRIPTION, description);
        click("Add wishlist button");
        populate("Wishlist title", title);
        populate("Wishlist description", description);
        click("Save wishlist button");
    }

    @Given("new wish is created")
    public void newWishIsCreated() {
        deleteUsersItems(TEST_USER_ID);
        getMainDashboardPage().waitAllWishlistsOnDashboard(DURATION_10_SECONDS);
        getMainDashboardPage().getAllWishlistsOnDashboard().get(0).click();
        click("Add new wish button");
        populate("Wish name field", WishlistConstants.TITLE);
        populate("Link field", LINK);
        clear("Price field");
        populate("Price field", PRICE);
        populate("descriptionField", WishlistConstants.DESCRIPTION);
        click("Add wish button");
    }

    @When("user clicks Edit wishlist button of created wishlist")
    @SneakyThrows
    public void userClicksEditWishlistButtonOfCreatedWishlist() {
        Thread.sleep(3000);
        boolean wasFound = false;
        String title = getScenarioContext().getDataAsString(TITLE);
        String description = getScenarioContext().getDataAsString(DESCRIPTION);

        for (int i = 0; i < getMainDashboardPage().getAllWishlistsOnDashboard().size(); i++) {
            WebElement actualTitle = getMainDashboardPage().getAllWishlistsOnDashboard().get(i).findElement(By.cssSelector("p:nth-child(1)"));
            WebElement actualDescription = getMainDashboardPage().getAllWishlistsOnDashboard().get(i).findElement(By.cssSelector("p:nth-child(2)"));
            WebElement actualData = getMainDashboardPage().getAllWishlistsOnDashboard().get(i).findElement(By.cssSelector("div > p.sc-jgrIVw.bMFFtS"));
            if (actualTitle.getText().equals(title) & actualDescription.getText().equals(description) & actualData.getText().equals(LocalDate.now().toString())) {
                wasFound = true;
                getMainDashboardPage().getAllWishlistsOnDashboard().get(i).findElement(By.cssSelector("img.sc-iNGGwv.ihFhQq")).click();
            }
        }
        softly.assertThat(wasFound).describedAs("Created wishlist not found").isTrue();
    }
}