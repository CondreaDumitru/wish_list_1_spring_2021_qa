package stepdefs.frontEnd.general;

import context.ScenarioContext;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.extern.log4j.Log4j2;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static context.Fields.REGISTER_EMAIL;
import static dbservice.DBService.deleteUserByEmail;
import static utils.WishlistConstants.*;

@Log4j2
public class    AbilityToAccessDashboardSteps extends BaseSteps {

    @When("Registration form is populated with data")
    public void populateRegistrationForm(DataTable table) {
        List<Map<String, String>> row = table.asMaps(String.class, String.class);
        getRegisterPage().waitTillFullNameClickable(DURATION_20_SECONDS);
        for (Map<String, String> stringStringMap : row) {
            populate("Full name Field", stringStringMap.get("Full Name"));
            populate("Email register field", stringStringMap.get("Email"));
            populate("Password register field", stringStringMap.get("Password"));
            populate("Confirm password field", stringStringMap.get("ConfirmPassword"));
            ScenarioContext.getScenarioContext().saveData(REGISTER_EMAIL, stringStringMap.get("Email"));
        }
    }

    @Then("User is redirected to Main Dashboard")
    public void checkDashboardPageIsAccessed() {
        getMainDashboardPage().waitAddWishlistDisplayed(DURATION_20_SECONDS);
        softly.assertThat(driver.getCurrentUrl())
                .describedAs("Dashboard page is not accessed")
                .isEqualTo(BASE_URL + MAIN_DASHBOARD_END_POINT);
        deleteUserByEmail(ScenarioContext.getScenarioContext().getDataAsString(REGISTER_EMAIL));
    }

    @When("Login form is populated with data")
    public void populateLoginForm(DataTable table) {
        List<Map<String, String>> row = table.asMaps(String.class, String.class);
        for (Map<String, String> stringStringMap : row) {
            Optional.ofNullable(stringStringMap.get("Email")).ifPresent(value -> populate("Email login field", value));
            Optional.ofNullable(stringStringMap.get("Password")).ifPresent(value -> populate("Password login field", value));
        }
    }

    @Then("User is not redirected to Main Dashboard")
    public void checkDashboardPageNotAccessed() {
        getHeaderComponent().waitRegisterButtonClickable(DURATION_20_SECONDS);
        log.info(driver.getCurrentUrl());
        softly.assertThat(driver.getCurrentUrl())
                .describedAs("Dashboard page is accessed").isNotEqualTo(BASE_URL + MAIN_DASHBOARD_END_POINT);
    }
}