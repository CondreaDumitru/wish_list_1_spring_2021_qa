package stepdefs.frontEnd.general;

import components.AddItemComponent;
import components.FooterComponent;
import components.HeaderComponent;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.WebDriver;
import pages.*;

import static utils.WebDriverInitializer.getWebInitInstance;
import static utils.WebElementFinder.getWebElementFieldOfName;
import static utils.WishlistConstants.BASE_URL;

@Getter
@Log4j2
public class BaseSteps {

    protected static SoftAssertions softly;
    protected static WebDriver driver;
    private final LandingPage landingPage = new LandingPage(driver);
    private final LoginPage loginPage = new LoginPage(driver);
    private final RegisterPage registerPage = new RegisterPage(driver);
    private final WishListPage wishListPage = new WishListPage(driver);
    private final MainDashboardPage mainDashboardPage = new MainDashboardPage(driver);
    private final HeaderComponent headerComponent = new HeaderComponent(driver);
    private final FooterComponent footerComponent = new FooterComponent(driver);
    private final AddItemComponent addItemComponent = new AddItemComponent(driver);

    public static void navigateToMainPage() {
        driver.navigate().to(BASE_URL);
    }

    @SneakyThrows
    public static void click(String buttonName) {
        getWebElementFieldOfName(buttonName, driver).click();
    }

    @SneakyThrows
    public static void populate(String fieldName, String keys) {
        getWebElementFieldOfName(fieldName, driver).sendKeys(keys);
    }

    public static void clear(String fieldName){
        getWebElementFieldOfName(fieldName, driver).clear();
    }

    public static boolean isPresent(String name) {
        return getWebElementFieldOfName(name, driver).isDisplayed();
    }

    public static boolean isEnabled(String name){
        return getWebElementFieldOfName(name, driver).isEnabled();
    }

    public static String getAttribute(String name,String attribute){
        return getWebElementFieldOfName(name, driver).getAttribute(attribute);
    }

    public static String getText(String name) {
        return getWebElementFieldOfName(name, driver).getText();
    }

    public void setDriver() {
        driver = getWebInitInstance().getDriver();
        driver.manage().window().maximize();
        softly = getWebInitInstance().getAssert();
    }
}