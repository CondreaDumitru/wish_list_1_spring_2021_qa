package stepdefs.frontEnd.general;

import io.cucumber.java.en.Then;

import static utils.WishlistConstants.WEBSITE_TITLE;

public class AccessWishListSteps extends BaseSteps {

    @Then("user is accessed to website")
    public void userIsAccessedToWebsite() {
        softly.assertThat(driver.getTitle())
                .describedAs("The title does not match the expected one.")
                .contains(WEBSITE_TITLE);
    }

    @Then("all LandingPage elements are present")
    public void allLandingPageElementsArePresent() {
        softly.assertThat(isPresent("Header"))
                .describedAs("Header is not present at landing page.")
                .isTrue();
        softly.assertThat(isPresent("Logo on header"))
                .describedAs("Logo is not present at landing page.")
                .isTrue();
        softly.assertThat(isPresent("Site name label"))
                .describedAs("NameLabel is not present at landing page.")
                .isTrue();
        softly.assertThat(isPresent("Main page button"))
                .describedAs("MainPageButton is not present at landing page.")
                .isTrue();
        softly.assertThat(isPresent("About us button"))
                .describedAs("AboutUsButton is not present at landing page.")
                .isTrue();
        softly.assertThat(isPresent("Register button"))
                .describedAs("RegisterButton is not present at landing page.")
                .isTrue();
        softly.assertThat(isPresent("Login button"))
                .describedAs("LoginButton is not present at landing page.")
                .isTrue();
        softly.assertThat(isPresent("Footer"))
                .describedAs("Footer is not present at landing page.")
                .isTrue();
        softly.assertThat(isPresent("Copyright"))
                .describedAs("Copyright is not present at landing page.")
                .isTrue();
        softly.assertThat(isPresent("Facebook button"))
                .describedAs("FacebookButton is not present at landing page.")
                .isTrue();
        softly.assertThat(isPresent("Instagram button"))
                .describedAs("InstagramButton is not present at landing page.")
                .isTrue();
        softly.assertThat(isPresent("Email contact"))
                .describedAs("Contact email is not present at landing page.")
                .isTrue();
        softly.assertThat(isPresent("General label"))
                .describedAs("GeneralLabel is not present at landing page.")
                .isTrue();
        softly.assertThat(isPresent("Explicative label"))
                .describedAs("ExplicativeLabel is not present at landing page.")
                .isTrue();
    }
}