package stepdefs.frontEnd.general;

import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j2;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.time.LocalDate;

import static context.Fields.DESCRIPTION;
import static context.Fields.TITLE;
import static context.ScenarioContext.getScenarioContext;

@Log4j2
public class DisplayWishlistsSteps extends BaseSteps {

    @Then("created wishlist is displayed with properly info")
    public void createdWishlistIsDisplayedWithProperlyInfo() {
        boolean wasFound = false;
        String title = getScenarioContext().getDataAsString(TITLE);
        String description = getScenarioContext().getDataAsString(DESCRIPTION);

        for (int i = 0; i < getMainDashboardPage().getAllWishlistsOnDashboard().size(); i++) {
            WebElement actualTitle = getMainDashboardPage().getAllWishlistsOnDashboard().get(i)
                    .findElement(By.cssSelector("p:nth-child(1)"));
            WebElement actualDescription = getMainDashboardPage().getAllWishlistsOnDashboard().get(i)
                    .findElement(By.cssSelector("p:nth-child(2)"));
            WebElement actualData = getMainDashboardPage().getAllWishlistsOnDashboard().get(i)
                    .findElement(By.cssSelector("div > p.sc-jgrIVw.bMFFtS"));
            if (actualTitle.getText().equals(title)) {
                wasFound = true;
                softly.assertThat(actualDescription.getText())
                        .describedAs("Description of created wishlist doesn't match").isEqualTo(description);
                softly.assertThat(actualData.getText())
                        .describedAs("Data of created wishlist doesn't match").isEqualTo(LocalDate.now().toString());
            }
        }
        softly.assertThat(wasFound).describedAs("Created wishlist not found").isTrue();
    }

    @Then("on each wishlist is displayed Edit button")
    public void onEachWishlistIsDisplayedEditButton() {
        for (int i = 0; i < getMainDashboardPage().getAllWishlistsOnDashboard().size(); i++) {
            getMainDashboardPage().getAllWishlistsOnDashboard().get(i).findElement(By.cssSelector("img.sc-iNGGwv.ihFhQq")).isDisplayed();
        }
    }
}