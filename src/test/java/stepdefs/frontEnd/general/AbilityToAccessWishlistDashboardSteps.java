package stepdefs.frontEnd.general;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import static stepdefs.backEnd.general.Requests.addWishlistForUser;
import static utils.WishlistConstants.*;

public class AbilityToAccessWishlistDashboardSteps extends BaseSteps {

    @Given("Wishlist is created")
    public void wishlistIsCreated() {
        addWishlistForUser(TEST_USER_EMAIL, WISHLIST_TITLE, WISHLIST_DESCRIPTION);
    }

    @Then("user is on Wishlist page")
    public void userIsRedirectedToWishlistDashboard() {
        softly.assertThat(driver.getCurrentUrl())
                .describedAs("The user is not redirected on the WishList Dashboard.")
                .isEqualTo(BASE_URL + WISHLIST_MAIN_DASHBOARD_END_POINT);
    }

    @Then("[Add new item] button is displayed")
    public void addNewItemButtonIsDisplayed() {
        softly.assertThat(isPresent("Add new wish button"))
                .describedAs("Add Wishlist form is not displayed")
                .isTrue();
    }
}
