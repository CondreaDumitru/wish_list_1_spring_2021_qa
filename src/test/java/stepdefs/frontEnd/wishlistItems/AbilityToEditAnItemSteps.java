package stepdefs.frontEnd.wishlistItems;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import stepdefs.frontEnd.general.BaseSteps;

import java.util.Map;
import java.util.Optional;

public class AbilityToEditAnItemSteps extends BaseSteps {

    @Then("Edit item form with all fields is displayed")
    public void editItemFormWithAllFieldsIsDisplayed() {
        softly.assertThat(isPresent("itemName"))
                .describedAs("Item name is not displayed")
                .isTrue();
        softly.assertThat(isPresent("itemLink"))
                .describedAs("Item link is not displayed")
                .isTrue();
        softly.assertThat(isPresent("itemPrice"))
                .describedAs("Item price is not displayed")
                .isTrue();
        softly.assertThat(isPresent("itemCurrency"))
                .describedAs("Item currency is not displayed")
                .isTrue();
        softly.assertThat(isPresent("itemDescription"))
                .describedAs("Item description is not displayed")
                .isTrue();
        softly.assertThat(isPresent("itemPriority"))
                .describedAs("Item priority is not displayed")
                .isTrue();
        softly.assertThat(isPresent("saveItemButton"))
                .describedAs("Save item button is not displayed")
                .isTrue();
    }

    @When("user fills Edit item form")
    public void userFillsEditItemForm(Map<String, String> values) {
        Optional.ofNullable(values.get("itemName")).ifPresent(value -> populate("Item name", value));
        Optional.ofNullable(values.get("itemLink")).ifPresent(value -> populate("Link", value));
        Optional.ofNullable(values.get("itemPrice")).ifPresent(value -> populate("Price", value));
        Optional.ofNullable(values.get("itemCurrency")).ifPresent(value -> populate("Currency", value));
        Optional.ofNullable(values.get("itemPriority")).ifPresent(value -> populate("Description", value));
        Optional.ofNullable(values.get("saveItemButton")).ifPresent(value -> populate("Priority", value));
    }

    @Then("the item is updated")
    public void theItemIsUpdated() {
    }

    @Given("{string} form is displayed")
    public void editItemFormIsDisplayed() {
        softly.assertThat(isPresent("itemForm"))
                .describedAs("Item form is not displayed");
    }

    @Then("the error is displayed for the required fields")
    public void theErrorIsDisplayedForTheRequiredFields() {
    }

    @Then("item added successfully")
    public void itemAddedSuccessfully() {
        softly.assertThat(getWishListPage().getItemsPanels().size())
                .describedAs("No item was added")
                .isEqualTo(1);
    }
}
