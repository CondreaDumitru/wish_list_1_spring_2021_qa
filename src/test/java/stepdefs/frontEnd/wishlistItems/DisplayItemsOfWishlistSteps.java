package stepdefs.frontEnd.wishlistItems;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import stepdefs.frontEnd.general.BaseSteps;

import java.util.List;

import static utils.WishlistConstants.*;

@Log4j2
public class DisplayItemsOfWishlistSteps extends BaseSteps {

    @Then("wish elements are displayed")
    public void contentOfItem(DataTable itemInfo) {
        getWishListPage().waitItemTitleDisplayed(DURATION_10_SECONDS);
        List<String> infoRows = itemInfo.asList();
        for (String row : infoRows) {
            softly.assertThat(isPresent(row)).
                    describedAs(row + "Element is not present").isTrue();
            log.info(row + " Element is present");
        }
    }

    @Then("all wishlist related items are displayed")
    public void itemsDisplayed() {
        getWishListPage().waitItemTitleDisplayed(DURATION_10_SECONDS);
        softly.assertThat(getText("Item title")).describedAs("Item title doesn't match").isEqualTo(TITLE);
        softly.assertThat(getText("Item description")).describedAs("Item description doesn't match").isEqualTo(DESCRIPTION);
        softly.assertThat(getText("Item price")).describedAs("Item price doesn't match").isEqualTo(PRICE + " MDL");
        softly.assertThat(getText("View priority button")).describedAs("Item priority doesn't match").isEqualTo("must have");
        log.info("All of the item/wish info matches");
    }

    @SneakyThrows
    @Then("user is redirected to linked page of product")
    public void redirectionToLinkedPage() {
        softly.assertThat(driver.getCurrentUrl()).describedAs("Redirected URL doesn't match").isEqualTo(LINK);
        log.info("User is redirected to linked page successfully");
    }

    @SneakyThrows
    @Then("selected item is deleted")
    public void checkItemsDeleted() {
        softly.assertThat(getWishListPage().getItemsPanels().size()).describedAs("Item is not deleted").isEqualTo(0);
        log.info("Items are successfully deleted");
    }
}
