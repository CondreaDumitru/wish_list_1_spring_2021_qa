package stepdefs.frontEnd.wishlistItems;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import lombok.SneakyThrows;
import stepdefs.frontEnd.general.BaseSteps;

import java.util.List;

import static utils.WishlistConstants.DURATION_10_SECONDS;

public class DisplayAddNewItemSteps extends BaseSteps {

    @When("user navigates to specific Wishlist Dashboard")
    public void navigateToWishlstDashbrd() {
        getMainDashboardPage().waitAllWishlistsOnDashboard(DURATION_10_SECONDS);
        getMainDashboardPage().getAllWishlistsOnDashboard().get(0).click();
        softly.assertThat(isPresent("addNewWishButton")).describedAs("User is not on Wishlist dashboard").isTrue();
    }

    @SneakyThrows
    @Then("\"Add new item\" form contains elements")
    public void containsElements(DataTable elements) {
        List<List<String>> rows = elements.asLists();
        for (List row : rows) {
            for (Object element : row) {
                softly.assertThat(isPresent(element.toString())).
                        describedAs(element + " element is not displayed").isTrue();
            }
        }
    }

    @Then("{string} contains following content")
    public void contentOfDropdown(String dropdownName, DataTable content) {
        List<String> contentRows = content.asList();
        StringBuilder expectedResult = new StringBuilder();
        for (String row : contentRows) {
            expectedResult.append(row).append("\n");
        }
        softly.assertThat(getText(dropdownName) + "\n").
                describedAs("Content of dropdown is not equal to expected result").
                isEqualTo(expectedResult.toString());
    }
}
