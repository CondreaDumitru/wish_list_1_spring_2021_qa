package stepdefs.frontEnd.wishlists;

import io.cucumber.java.en.Then;
import stepdefs.frontEnd.general.BaseSteps;

import java.time.LocalDate;

import static context.Fields.DESCRIPTION;
import static context.Fields.TITLE;
import static context.ScenarioContext.getScenarioContext;
import static utils.WishlistConstants.DURATION_10_SECONDS;

public class EditWishlistFormSteps extends BaseSteps {

    @Then("Edit wishlist form with all fields are displayed")
    public void editWishlistFormWithAllFieldsAreDisplayed() {
        getMainDashboardPage().getEditWishlistComponent().waitDeleteButtonDisplayed(DURATION_10_SECONDS);
        softly.assertThat(isPresent("Edit wishlist form"))
                .describedAs("Edit Wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Close edit form x button"))
                .describedAs("Close edit  form X button from Wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Edit Wishlist Title"))
                .describedAs("Title from edit wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Edit Wishlist type"))
                .describedAs("Type from edit wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Edit Wishlist title"))
                .describedAs("Title from edit wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Edit Wishlist date"))
                .describedAs("Date from edit wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Edit Wishlist description"))
                .describedAs("Description from edit wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Edit Wishlist privacy"))
                .describedAs("Privacy from edit wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Save changes button"))
                .describedAs("Save changes button from edit wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Delete Wishlist button"))
                .describedAs("Delete wishlist button is not present")
                .isTrue();
    }

    @Then("each field contains proper info")
    public void eachFieldContainsProperInfo() {
        softly.assertThat(getAttribute("Edit wishlist title", "value"))
                .describedAs("Title in edit form does not match with created wishlist's title")
                .isEqualTo(getScenarioContext().getDataAsString(TITLE));
        softly.assertThat(getAttribute("Edit wishlist description", "value"))
                .describedAs("Description in edit form does not match with created wishlist's description")
                .isEqualTo(getScenarioContext().getDataAsString(DESCRIPTION));
        softly.assertThat(getAttribute("Edit wishlist date", "placeholder"))
                .describedAs("Date in edit form does not match with created wishlist's date")
                .isEqualTo(LocalDate.now().toString());
    }
}