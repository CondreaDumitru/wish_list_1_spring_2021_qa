package stepdefs.frontEnd.wishlists;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import stepdefs.frontEnd.general.BaseSteps;

import java.util.Map;
import java.util.Optional;

public class AddWishlistUISteps extends BaseSteps {

    @Then("Add Wishlist form with all fields are displayed")
    public void addWishlistFormWithAllFieldsAreDisplayed() {
        softly.assertThat(isPresent("Wishlist form"))
                .describedAs("Add Wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Close form x button"))
                .describedAs("Close form X button from Wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Wishlist Title"))
                .describedAs("Title from wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Wishlist type"))
                .describedAs("Type from wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Wishlist title"))
                .describedAs("Title from wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Wishlist date"))
                .describedAs("Date from wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Wishlist description"))
                .describedAs("Description from wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Wishlist privacy"))
                .describedAs("Privacy from wishlist form is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Save wishlist button"))
                .describedAs("Save Wishlist from wishlist form is not displayed")
                .isTrue();
    }

    @When("user fill Add New Wishlist form")
    public void userFillAddNewWishlistForm(Map<String, String> credentials) {
        Optional.ofNullable(credentials.get("Title")).ifPresent(value -> populate("Wishlist Title", value));
        Optional.ofNullable(credentials.get("Description")).ifPresent(value -> populate("Wishlist Description", value));
    }

    @Then("Save wishlist button is not clickable")
    public void saveWishlistButtonIsNotClickable() {
        softly.assertThat(isEnabled("Save wishlist button"))
                .describedAs("Save wishlist button is clickable")
                .isFalse();
    }
}