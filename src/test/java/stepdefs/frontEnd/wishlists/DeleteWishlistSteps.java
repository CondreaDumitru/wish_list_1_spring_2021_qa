package stepdefs.frontEnd.wishlists;

import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import stepdefs.frontEnd.general.BaseSteps;

import static context.Fields.TITLE;
import static context.ScenarioContext.getScenarioContext;
import static utils.WishlistConstants.*;

public class DeleteWishlistSteps extends BaseSteps {

    @Then("delete wishlist message is: \"Are you sure you want to delete this wishlist?\"")
    public void deletePopUpMessageIsAreYouSureYouWantToDeleteThisWishlist() {
        softly.assertThat(getText("Edit wishlist component"))
                .describedAs("Delete wishlist message does not match the expected one")
                .isEqualTo(DELETE_WISHLIST_MESSAGE);
    }

    @Then("wishlist is deleted")
    public void wishlistDeleted() {

        boolean wasFound = false;
        for (int i = 0; i < getMainDashboardPage().getAllWishlistsOnDashboard().size(); i++) {
            WebElement actualTitle = getMainDashboardPage().getAllWishlistsOnDashboard()
                    .get(i).findElement(By.cssSelector("p:nth-child(1)"));

            if (actualTitle.getText().equals(getScenarioContext().getDataAsString(TITLE))) {
                wasFound = true;
            }

            softly.assertThat(wasFound).describedAs("Wishlist is not deleted").isFalse();
        }
    }

    @Then("pop-up is closed and user is redirected to Main Dashboard")
    public void popUpIsClosedAndUserIsRedirectedToMainDashboard() {
        softly.assertThat(isPresent("Edit wishlist component"))
                .describedAs("Delete pop-up did not close")
                .isFalse();

        softly.assertThat(driver.getCurrentUrl())
                .describedAs("User is not redirected to main dashboard.")
                .isEqualTo(BASE_URL + MAIN_DASHBOARD_END_POINT);

    }
}