package stepdefs.frontEnd.register;

import context.ScenarioContext;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import stepdefs.frontEnd.general.BaseSteps;

import java.util.Map;
import java.util.Optional;

import static context.Fields.REGISTER_EMAIL;
import static dbservice.DBService.deleteUserByEmail;
import static utils.WishlistConstants.*;

public class RegisterSteps extends BaseSteps {

    @When("user fill Register form")
    public void userFillRegisterForm(Map<String, String> credentials) {
        Optional.ofNullable(credentials.get("Full Name")).ifPresent(value -> populate("Full name Field", value));
        Optional.ofNullable(credentials.get("Email")).ifPresent(value -> populate("Email register field", value));
        Optional.ofNullable(credentials.get("Password")).ifPresent(value -> populate("Password register field", value));
        Optional.ofNullable(credentials.get("Confirm Password")).ifPresent(value -> populate("Confirm password field", value));

        ScenarioContext.getScenarioContext().saveData(REGISTER_EMAIL, credentials.get("Email"));
    }

    @Then("Create Account button is not clickable")
    public void buttonIsNotClickable() {
        softly.assertThat(isEnabled("Create account"))
                .describedAs("Create Account button is clickable")
                .isFalse();
    }

    @Then("Create Account button is clickable")
    public void createAccountButtonIsClickable() {
        getRegisterPage().waitTillCreateAccountClickable(DURATION_10_SECONDS);
        softly.assertThat(isEnabled("Create account"))
                .describedAs("Create Account button is not clickable")
                .isTrue();
    }

    @Then("the user is logged in")
    public void theUserIsLoggedIn() {
        getMainDashboardPage().waitAddWishlistDisplayed(DURATION_10_SECONDS);
        softly.assertThat(driver.getCurrentUrl())
                .describedAs("The user is not logged in")
                .isEqualTo(BASE_URL + MAIN_DASHBOARD_END_POINT);
        deleteUserByEmail((String) ScenarioContext.getScenarioContext().getDataFrom(REGISTER_EMAIL));
    }
}