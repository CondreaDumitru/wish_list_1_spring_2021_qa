package stepdefs.frontEnd.register;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j2;
import stepdefs.frontEnd.general.BaseSteps;

import static utils.WebDriverInitializer.getWebInitInstance;
import static utils.WishlistConstants.*;

@Log4j2
public class DisplayingRegisterPageSteps extends BaseSteps {

    @Given("landing page is navigated")
    public void landingPageIsNavigated() {
        navigateToMainPage();
        log.info("Landing page is displayed");
    }

    @Then("is displayed register page")
    public void idDisplayedRegisterPage() {
        getHeaderComponent().waitMainPageClickable(DURATION_10_SECONDS);

        softly.assertThat(getWebInitInstance().getDriver().getCurrentUrl())
                .describedAs("Register page is not displayed")
                .isEqualTo(BASE_URL + REGISTER_END_POINT);
    }

    @Then("all elements from register page are displayed")
    public void eachFieldIsDisplayed() {
        softly.assertThat(isPresent("Full name field"))
                .describedAs("Full Name field is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Email register field"))
                .describedAs("Email field is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Password register field"))
                .describedAs("Password field is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Confirm password field"))
                .describedAs("Confirm password field is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Create account"))
                .describedAs("Create account button is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Login link"))
                .describedAs("Login link field is not displayed")
                .isTrue();
    }
}