package stepdefs.frontEnd.register;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j2;
import stepdefs.frontEnd.general.BaseSteps;

import static utils.WebDriverInitializer.getWebInitInstance;
import static utils.WishlistConstants.*;

@Log4j2
public class AccessRegisterFormSteps extends BaseSteps {

    @Given("[Register] button is present")
    public void checkRegisterButton() {
        softly.assertThat(isPresent("Register button"))
                .describedAs("[Register] button is present")
                .isTrue();
        softly.assertThat(getText("Register button"))
                .describedAs("[Register Now] button text matches")
                .contains(REGISTER_BUTTON_NAME);

        log.info("MDDIP006-10091 | Register button is present and displayed correctly");
    }

    @Then("user is redirected to page with \"Register\" form")
    public void checkRedirectionToRegister() {
        softly.assertThat(getWebInitInstance().getDriver().getCurrentUrl())
                .describedAs("Redirected URL doesn't match")
                .isEqualTo(BASE_URL + REGISTER_END_POINT);

        log.info("MDDIP006-10091 | User is successfully redirected to page with register form");
    }
}