package stepdefs.frontEnd.login;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import stepdefs.frontEnd.general.BaseSteps;

import java.util.Map;

import static utils.WishlistConstants.*;

public class LogInSteps extends BaseSteps {

    @When("user fills Login form")
    public void userFillLoginForm(Map<String, String> credentials) {
        populate("Email login field", credentials.get("Email"));
        populate("Password login field", credentials.get("Password"));
    }

    @Then("user is logged in")
    public void userIsLoggedIn() {
        getHeaderComponent().waitDashboardButtonDisplayed(DURATION_10_SECONDS);
        softly.assertThat(driver.getCurrentUrl())
                .describedAs("The user is logged in.")
                .isEqualTo(BASE_URL + MAIN_DASHBOARD_END_POINT);
    }

    @Then("Submit button is not clickable")
    public void submitButtonIsNotClickable() {
        softly.assertThat(isEnabled("Submit"))
                .describedAs("Submit button is clickable")
                .isFalse();
    }
}