package stepdefs.frontEnd.login;

import io.cucumber.java.en.Then;
import stepdefs.frontEnd.general.BaseSteps;

import static utils.WishlistConstants.*;

public class AccessLoginFormSteps extends BaseSteps {

    @Then("user is accessed to website login form")
    public void userIsAccessedToWebsiteLoginForm() {
        softly.assertThat(driver.getCurrentUrl())
                .describedAs("Current URL does not match the expected one")
                .isEqualTo(BASE_URL + LOGIN_END_POINT);

        softly.assertThat(getText("Login page label"))
                .describedAs("Login form label does not match the expected one")
                .isEqualTo(LOGIN_FORM_LABEL);
    }
}