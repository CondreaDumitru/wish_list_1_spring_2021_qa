package stepdefs.frontEnd.login;

import io.cucumber.java.en.Then;
import lombok.extern.log4j.Log4j2;
import stepdefs.frontEnd.general.BaseSteps;

import static utils.WishlistConstants.*;

@Log4j2
public class DisplayLoginFormSteps extends BaseSteps {

    @Then("Login form contains \"Email address\", \"Password\", [Login] button")
    public void checkLoginFormContains() {
        softly.assertThat(isPresent("Login page label"))
                .describedAs("Login page label is not displayed")
                .isTrue();
        softly.assertThat(getText("Login page label"))
                .describedAs("Login page label text doesn't match")
                .contains(LOGIN_FORM_LABEL);
        softly.assertThat(isPresent("Email login field"))
                .describedAs("Email field is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Password login field"))
                .describedAs("Password field is not displayed")
                .isTrue();
        softly.assertThat(isPresent("Submit"))
                .describedAs("[Log In] button is not displayed")
                .isTrue();
        softly.assertThat(getText("Submit"))
                .describedAs("Submit button text is not right")
                .contains(LOGIN_SUBMIT_BUTTON_TEXT);
        softly.assertThat(isPresent("Register now button"))
                .describedAs("[Register Now] button is not displayed")
                .isTrue();
        softly.assertThat(getText("Register now button"))
                .describedAs("[Register Now] button text doesn't match")
                .contains(REGISTER_NOW_BUTTON_TEXT_AT_LOGIN);
    }
}