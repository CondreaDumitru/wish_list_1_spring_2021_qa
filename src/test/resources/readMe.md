Business value:
================
The created service allows the creation of wishlists. A wish list is an itemization, or an enumeration of goods or
services that a person or organization wants. The author, is the user who creates a wish list, can add wishes to the
list and later distribute the list to the family, friends and other stakeholders who are inclined to buy gifts for the
future recipient or offer some items listed, for sale.

Business need
--------------
The purpose of a wish list is to facilitate communication between the recipient of the gift and the gift giver.

Frontend test environment: <https://wishlist-app-frontend-test.herokuapp.com/>
Backend test environment: <https://wishlist-app-backend-test.herokuapp.com/>
Frontend dev environment: <https://wishlist-app-frontend.herokuapp.com/>
Backend dev environment: <https://wishlist-app-backend.herokuapp.com/>