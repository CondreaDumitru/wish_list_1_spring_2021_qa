@MDDIP006-10097 @AccessMainDashboard @vgurov @FE
Feature: Ability to access the Main Dashboard

  Scenario: Check the redirection to Dashboard upon registration
    Given user is on register page
    When Registration form is populated with data
      | Full Name | Email             | Password     | ConfirmPassword |
      | TestName  | RegTest@Test.Test | TestTEST123! | TestTEST123!    |
    And user clicks on Create Account
    Then User is redirected to Main Dashboard

  Scenario: Check the redirection to Dashboard upon login
    Given user is on log in page
    When Login form is populated with data
      | Email          | Password     |
      | Test@Test.Test | TestTEST123! |
    And user clicks on submit
    Then User is redirected to Main Dashboard

  @Negative
  Scenario: Check that user is not redirected to Dashboard with failed login attempt
    Given user is on log in page
    When Login form is populated with data
      | Email                            | Password             |
      | nonexistent@RANDOM.GSHDJKFLAKSGF | nonexistentRANDOM123 |
    And user clicks on submit
    Then User is not redirected to Main Dashboard

  @Negative
  Scenario: Check that user is not redirected to Dashboard with failed registration attempt
    Given user is on register page
    When Registration form is populated with data
      | Full Name         | Email          | Password     | ConfirmPassword |
      | nonexistentRANDOM | Test@Test.Test | TestTEST123! | TestTEST123!    |
    And user clicks on  Create Account
    Then User is not redirected to Main Dashboard