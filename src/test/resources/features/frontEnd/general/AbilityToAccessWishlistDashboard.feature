@MDDIP006-10762 @gbarbaneagra @AccessWishlistDashboard @FE
Feature: Ability to access the wishlist dashboard

  Background: all the Wishlists are displayed on the Main Dashboard
    Given user is logged in with test account

  Scenario: Navigate to the WishList dashboard.
    Given Wishlist is created
    When user clicks on {wishList}
    Then user is on Wishlist page
    Then [Add new item] button is displayed
