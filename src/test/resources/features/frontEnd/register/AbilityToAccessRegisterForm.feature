@MDDIP006-10091 @abulat @RegisterPage @FE
Feature: Ability to access the register form
  As a customer, I should have the ability to access the Wishlist register form.
  Business need:
  The ability to access the register form by clicking on the the 'Register' button;
  Acceptance criteria:
  On clicking on the 'Register' button, I should be redirected to the Wishlist register form;

  Scenario: Redirecting to "Register" page by clicking on [Register] button
    Given user is on Main Page
    And [Register] button is present
    When user clicks on Register button
    Then user is redirected to page with "Register" form