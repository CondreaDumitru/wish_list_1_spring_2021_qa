@Register @FE
Feature: Ability to Register

  @MDDIP006-10092 @vscutelnic @UI
  Scenario: Check that UI Elements on register form are displayed
    Given landing page is navigated
    When user clicks on Register button
    Then is displayed register page
    And all elements from register page are displayed

  @MDDIP006-10092 @vscutelnic @Negative
  Scenario Outline: Check that creates account button is not clickable with empty mandatory fields
    Given user is on register page
    When user fill Register form
      | Full Name        | <Full Name>        |
      | Email            | <Email>            |
      | Password         | <Password>         |
      | Confirm Password | <Confirm Password> |
    Then Create Account button is not clickable

    Examples:
      | Full Name | Email          | Password | Confirm Password |
      | TestName  | test@email.com | Pass123  |                  |
      | TestName  | test@email.com |          | Pass123          |
      | TestName  |                | Pass123  | Pass123          |
      |           | test@email.com | Pass123  | Pass123          |

  @MDDIP006-10092 @vscutelnic
  Scenario Outline: Check that creates account button is clickable with filled mandatory fields
    Given user is on register page
    When user fill Register form
      | Full Name        | <Full Name>        |
      | Email            | <Email>            |
      | Password         | <Password>         |
      | Confirm Password | <Confirm Password> |
    Then Create Account button is clickable

    Examples:
      | Full Name | Email          | Password | Confirm Password |
      | TestName  | test@email.com | Pass123  | Pass123          |

  @MDDIP006-10093 @gbarbaneagra
  Scenario Outline: Successful register when the user enters valid data
    Given user is on register page
    When user fill Register form
      | Full Name        | <Full Name>        |
      | Email            | <Email>            |
      | Password         | <Password>         |
      | Confirm Password | <Confirm Password> |
    And user clicks on Create Account
    Then the user is logged in
    Examples:
      | Full Name            | Email            | Password                         | Confirm Password                 |
      | Acrobat-D'A.com,bien | test@emmail.com  | Pass123                          | Pass123                          |
      | TestName             | test@yamhoo.com  | Pas123                           | Pas123                           |
      | Dante                | test456@sm.com   | Pass1231234567897897421503564921 | Pass1231234567897897421503564921 |
      | I                    | email@emmail.com | Pass123                          | Pass123                          |