@MDDIP006-10101 @vscutelnic @Wishlists @FE @Run
Feature: User's wishlists is displayed on main dashboard

  Background: user is logged in on GoldWish
    Given user is logged in with test account

  Scenario: Check that all user's wishlist is displayed on main dashboard
    When new wishlist is created with title ThisTITLE000 and description ThisDescription
    Then created wishlist is displayed with properly info
    And on each wishlist is displayed Edit button