@MDDIP006-10098 @vscutelnic @MainDashboard @FE @Run
Feature: Button to add wishlist

  Background: user is logged in on GoldWish
    Given user is logged in with test account

  @UI
  Scenario: Check "Add wishlist" button is displayed on main dashboard page
    Then Add Wishlist button is displayed

  Scenario: Check that at click on "Add wishlist" button, is displayed new wishlist form
    When user clicks on Add wishlist button
    Then Wishlist form is displayed