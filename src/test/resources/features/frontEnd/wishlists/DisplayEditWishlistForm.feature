@MDDIP006-10099 @vscutelnic @AddWishlist @UI @FE @Run
Feature: Display the Edit Wishlist form

  Background: user is logged in on GoldWish
    Given user is logged in with test account
    And new wishlist is created with title ThisTITLE000 and description ThisDescription

  Scenario: Check that all elements from add wishlist form is displayed
    When user clicks Edit wishlist button of created wishlist
    Then Edit wishlist form with all fields are displayed
    And each field contains proper info
