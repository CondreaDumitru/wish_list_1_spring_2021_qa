@MDDIP006-10099 @vscutelnic @AddWishlist @UI @FE @Run
Feature: Display the Add Wishlist form

  Background: user is logged in on GoldWish
    Given user is logged in with test account

  Scenario: Check that all elements from add wishlist form are displayed
    When user clicks on Add Wishlist button
    Then Add Wishlist form with all fields are displayed

  @Negative
  Scenario Outline: Check that when mandatory fields are not filled, [Save wishlist] button is not clickable
    When user clicks on Add Wishlist button
    And user fill Add New Wishlist form
      | Title       | <Title>       |
      | Description | <Description> |
    Then Save wishlist button is not clickable

    Examples:
      | Title     | Description |
      | TestTitle |             |
      |           | Test        |
