@MDDIP006-10146 @Wishlist @dcondrea @FE
Feature: Ability to delete the wishlist

  As a customer, I should be able to delete the wishlist from the Main Dashboard.

  Acceptance criteria:
  - When clicking on the 'Delete wishlist' button - a pop-up should be displayed:
  - The message should be 'Are you sure you want to delete this wishlist?'
  - The options should be: 'Continue' and 'Cancel';
  - On 'Continue' - the wishlist should be deleted;
  - On 'Cancel' - the pop-up should be closed an customer should return to Main Dashboard;

  Background: user clicks on delete wishlist button
    Given user is logged in with test account
    And new wishlist is created with title ThisTITLE000 and description ThisDescription
    And user clicks Edit wishlist button of created wishlist
    And user clicks on Delete Wishlist Button

  Scenario: Check presence of pop-up message and [Continue] [Cancel] buttons.
    Then Continue Delete Button is displayed
    Then Cancel Delete Button is displayed
    Then Delete Wishlist Message is displayed
    Then delete wishlist message is: "Are you sure you want to delete this wishlist?"

  Scenario: delete wishlist
    When user clicks on Continue Delete Button
    Then wishlist is deleted

  Scenario: cancel deletion of wishlist
    When user clicks on Cancel Delete Button
    Then pop-up is closed and user is redirected to Main Dashboard