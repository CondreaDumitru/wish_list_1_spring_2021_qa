@MDDIP006-10095 @abulat @LoginPage @FE
Feature: Display the login form
  As a customer after clicking the 'Login' button
  I should be displayed with a login form
  To provide the credentials

  Acceptance criteria:
  - Create a separate page with login form;
  - Login form should have the following elements:
  --Email address;
  --Password;
  --Login button;

  Scenario: Redirecting to page that contains 'Login' form
    Given user is on Main Page
    When user clicks on Login button
    Then user is accessed to website login form
    And Login form contains "Email address", "Password", [Login] button
