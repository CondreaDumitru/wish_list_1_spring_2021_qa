@MDDIP006-10094 @dcondrea @LoginPage @FE
Feature: Ability to access the login form

  Scenario: Navigate to the WishList login form.
    Given user is on Main Page
    When user clicks on Login button
    Then user is accessed to website login form