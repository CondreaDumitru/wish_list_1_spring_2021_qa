@MDDIP006-10096 @Login @gbarbaneagra @FE
Feature: Verify ability to Login

  Scenario Outline: Successful login when the user logs in with <registered credentials>
    Given user is on log in page
    When user fills Login form
      | Email    | <Email>    |
      | Password | <Password> |
    And user clicks on submit
    Then user is logged in
    Examples:
      | Email          | Password     |
      | Test@Test.Test | TestTEST123! |

  @Negative
  Scenario Outline: Submit button is not clickable when the user logs in with <invalid email> and <valid password>
    Given user is on log in page
    When user fills Login form
      | Email    | <Email>    |
      | Password | <Password> |
    Then Submit button is not clickable
    Examples:
      | Email     | Password    |
      | asd       | Acrobat1234 |
      | asd@@     | Aaaa456     |
      | asd@maail | 789cADD     |

  @Negative
  Scenario Outline: Submit button is not clickable when the user logs in with <valid email> and <invalid password>
    Given user is on log in page
    When user fills Login form
      | Email    | <Email>    |
      | Password | <Password> |
    Then Submit button is not clickable
    Examples:
      | Email                | Password |
      | george@gmail.com     | asd      |
      | George.serg@mail.com | Asd@@    |
      | acr-sd@yahoo.fr      | 2        |

  @Negative
  Scenario Outline: Submit button is not clickable when the user logs in with <invalid email> and <invalid password>
    Given user is on log in page
    When user fills Login form
      | Email    | <Email>    |
      | Password | <Password> |
    Then Submit button is not clickable
    Examples:
      | Email                               | Password   |
      | com.com                             | test'test; |
      | a"b(c)d,e:f;g<h>i[j\k]l@example.com | .          |
      | QA[icon]CHOCOLATE[icon]@test.com    | 2345       |