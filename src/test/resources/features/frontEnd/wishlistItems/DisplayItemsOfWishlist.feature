@MDDIP006-11456 @abulat @WishListItem @FE
Feature: Display all the items on the Wishlist Dashboard
  As a customer which added one or more items to a specific Wishlist, I should be able to see all the items in one place.

  Acceptance criteria:
  1. The items should be displayed on the Wishlist Dashboard;
  2. On the Dashboard, each item should have the following set of information and options:
  Item name;
  Price;
  View button(See below the AC for the 'View' button);
  Delete button;
  2.1. If the URL/link was not added to the item - the 'View' button should not be displayed;
  3. When clicking on 'View' button - a new window should be opened with the URL/link that was previously added to the item;
  4. When clicking on 'Delete' button - a pop-up should be displayed with the following information:
  Verbiage: Are you sure you want to delete this item?
  Options: Confirm/Cancel
  4.1. On Confirm - the item should be deleted;
  4.2. On Cancel - the pop-up should be closed;

  Background: user is on Wishlist Dashboard
    Given user is logged in with test account
    And new wishlist is created with title ThisTITLE000 and description ThisDescription

  @UI
  Scenario: Item elements are present
    When new wish is created
    Then wish elements are displayed
      | Item title           |
      | Item Price           |
      | View Priority button |
      | Delete button        |

  Scenario: Wishlist dashboard displays all added items
    When new wish is created
    Then all wishlist related items are displayed

  Scenario: View button redirection
    When new wish is created
    And user clicks on View Priority button
    Then user is redirected to linked page of product

  Scenario: Deleting item
    When new wish is created
    And user clicks on Delete Button
    Then selected item is deleted

#  Not done by developers
#  Scenario: Cancelling deletion of item
#    When new wish is created
#    And user clicks on {Delete button}
#    And user clicks on {Cancel button}
#    Then selected item is not deleted and remains present in list
