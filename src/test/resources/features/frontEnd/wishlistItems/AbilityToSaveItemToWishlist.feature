@MDDIP006-11452 @WishlistItem @dcondrea @FE
Feature: Ability to save the item to wishlist

  As a customer that filled the information about the item,
  I should be able to save that item to wishlist.

  Acceptance criteria:
  Upon clicking on [Add item] button - perform the required fields check.
  - If the required fields where not filled - an error should be returned;
  - If all checks were successful - create/save the item;

  There is only one required field in the "Add New Wish" form - "Title".

  Background: user is on "Add New Wish" page
    Given user is logged in with test account
    And user is on Wishlist page
    And user clicks on Add New Wish button


  Scenario: Add a wish by filling required "Title" field
    When user populates Wish Name field with iPhone 12 Pro
    And user clicks on Add Wish button
    Then item added successfully

  @Negative
  Scenario: Add a wish without filling required "Title" field
    When user clicks on Add Wish button
    Then an error is returned
