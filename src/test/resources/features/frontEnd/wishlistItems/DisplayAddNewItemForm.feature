@MDDIP006-10769 @abulat @WishListItem @FE @RunNow
Feature: Display Add New Item form

  As a customer, I should be able to add items/gifts to each Wishlist with all
  the necessary information about them.

  Acceptance Criteria:
  1. When clicking 'Add new item' on the Wishlist Dashboard, the customer should be redirected to the item form;
  2. The 'Add new item' form should have the following fields/elements:
  Item name;
  Link;
  Price;
  Currency;
  Description;
  Priority;
  Add item;

  Background: user is on Wishlist Dashboard
    Given user is logged in with test account

  @UI
  Scenario: "Add new item" form contains all elements
    When user navigates to specific Wishlist Dashboard
    And user clicks on Add new wish button
    Then Add New Wish Form is displayed
    And "Add new item" form contains elements
      | Wish name label        | Wish name field    |
      | Link label             | Link field         |
      | Price label            | Price field        |
      | Currency label         | Currency drop down |
      | Description label      | Description field  |
      | Priority label         | Priority drop down |
      | Close Wish Form Button | Add wish button    |

  @UI
  Scenario: Content and default values of dropdowns in "Add new item" form
    When user navigates to specific Wishlist Dashboard
    And user clicks on Add new wish button
    Then Add new wish form is displayed
    And "Currency dropdown" contains following content
      | MDL |
      | EUR |
      | USD |
    And "Priority dropdown" contains following content
      | Must Have    |
      | Nice to Have |
      | Want to Have |

