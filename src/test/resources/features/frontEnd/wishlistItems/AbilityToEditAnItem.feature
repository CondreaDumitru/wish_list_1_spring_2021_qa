@MDDIP006-11458 @gbarbaneagra @AbilityToEditAnItem
Feature: Edit an item

  Background: user is logged in with test account, wishlist is created with an item
    Given user is logged in with test account
    And Wishlist is created
    When user clicks on {allWishlistsOnDashboard} button
    And user clicks on {addNewWish} button
    And user populates Wish Name field with iPhone 12 Pro
    And user clicks on Add Wish button
    Then item added successfully
    Then user is on Wishlist page

  Scenario: Access the 'Edit item' form
    When user clicks on {editButton}
    Then 'Edit item' form with all fields is displayed

  Scenario Outline: Successfully update an item
    Given Edit item form with all fields is displayed
    When user fills Edit item form
      | Item name   | <Item name>   |
      | Link        | <Link>        |
      | Price       | <Price>       |
      | Currency    | <Currency>    |
      | Description | <Description> |
      | Priority    | <Priority>    |
    And user clicks on {Update} button
    Then the item is updated
    Examples:
      | Item name | Link                   | Price | Currency | Description  | Priority  |
      | Iphone 12 | https://www.apple.com/ | 1400  | $(USD)   | Gold, 512 GB | Must Have |
      | MacAir    | https://www.apple.com/ | 2800  | $(USD)   | Grey         | Must Have |

  @Negative
  Scenario Outline: Unsuccessfully update an item
    Given 'Edit item' form is displayed
    When user fills Edit item form
      | Item name   | <Item name>   |
      | Link        | <Link>        |
      | Price       | <Price>       |
      | Currency    | <Currency>    |
      | Description | <Description> |
      | Priority    | <Priority>    |
    And user clicks on {Update} button
    Then the error is displayed for the required fields
    Examples:
      | Item name | Link                   | Price | Currency | Description  | Priority  |
      |           | https://www.apple.com/ | 1400  | $(USD)   | Gold, 512 GB | Must Have |
      | Iphone 12 |                        | 1400  | $(USD)   | Gold, 512 GB | Must Have |
      | Iphone 12 | https://www.apple.com/ |       | $(USD)   | Gold, 512 GB | Must Have |
      | Iphone 12 | https://www.apple.com/ | 1400  |          | Gold, 512 GB | Must Have |
      | Iphone 12 | https://www.apple.com/ | 1400  | $(USD)   |              | Must Have |
      | Iphone 12 | https://www.apple.com/ | 1400  | $(USD)   | Gold, 512 GB |           |
