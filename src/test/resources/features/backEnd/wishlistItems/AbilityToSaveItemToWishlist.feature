@MDDIP006-11452 @WishlistItem @dcondrea @BE
Feature: Ability to save the item to wishlist

  As a customer that filled the information about the item,
  I should be able to save that item to wishlist.

  Acceptance criteria:
  Upon clicking on [Add item] button - perform the required fields check.
  - If the required fields where not filled - an error should be returned;
  - If all checks were successful - create/save the item;

  There is only one required field in the "Add New Wish" form - "Title".

  Background: Create test wishlist
    Given Wishlist is created

  Scenario: Add a wish by filling required "Title" field
    Given user send post request to add a wish by filling "Title" field
    Then user get response with code 202

  @Negative
  Scenario: Add a wish without filling required "Title" field
    Given user send post request to add a wish without filling "Title" field
    Then user get response with code 400