@BE @vscutelnic @Wishlists
Feature: Return user wishlists

  Scenario: Check that when user sent request, wishlist is saved, and when user wants to get wishlists, they are returned
    Given user send post request to save wishlist
    Then at get request wishlist with saved data is returned